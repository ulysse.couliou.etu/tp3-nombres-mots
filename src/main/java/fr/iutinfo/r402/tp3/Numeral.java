package fr.iutinfo.r402.tp3;

import java.util.HashMap;

public class Numeral {
    private final String number;
    private static final HashMap<Integer, String> units = new HashMap<>();
    private static final HashMap<Integer, String> tens = new HashMap<>();
    private static final HashMap<Integer, String> hundreds = new HashMap<>();
    private static final HashMap<Integer, String> thousands = new HashMap<>();
    private static final HashMap<Integer, String> otherUnits = new HashMap<>();

    static {
        units.put(0, "zéro");
        units.put(1, "un");
        units.put(2, "deux");
        units.put(3, "trois");
        units.put(4, "quatre");
        units.put(5, "cinq");
        units.put(6, "six");
        units.put(7, "sept");
        units.put(8, "huit");
        units.put(9, "neuf");

        tens.put(10, "dix");
        tens.put(11, "onze");
        tens.put(12, "douze");
        tens.put(13, "treize");
        tens.put(14, "quatorze");
        tens.put(15, "quinze");
        tens.put(16, "seize");
        tens.put(20, "vingt");
        tens.put(30, "trente");
        tens.put(40, "quarante");
        tens.put(50, "cinquante");
        tens.put(60, "soixante");
        tens.put(70, "soixante-dix");
        tens.put(80, "quatre-vingts");

        hundreds.put(100, "cent");
        hundreds.put(200, "deux cents");
        hundreds.put(300, "trois cents");
        hundreds.put(400, "quatre cents");
        hundreds.put(500, "cinq cents");
        hundreds.put(600, "six cents");
        hundreds.put(700, "sept cents");
        hundreds.put(800, "huit cents");
        hundreds.put(900, "neuf cents");

        thousands.put(1000, "mille");
        thousands.put(2000, "deux mille");
        thousands.put(3000, "trois mille");
        thousands.put(4000, "quatre mille");
        thousands.put(5000, "cinq mille");
        thousands.put(6000, "six mille");
        thousands.put(7000, "sept mille");
        thousands.put(8000, "huit mille");
        thousands.put(9000, "neuf mille");

        otherUnits.put(1000000, "million");
        otherUnits.put(1000000000, "milliard");
    }

    public Numeral(String number) {
        this.number = number;
    }

    public String toLetters() {
        int num = Integer.parseInt(number);
        StringBuilder result = new StringBuilder();

        if (num % 100 == 21 && num != 21) {
            handleSpecialCase(num, result);
        } else if (num < 10) {
            result.append(units.get(num));
        } else if (num < 17 || (num < 70 && num % 10 == 0)) {
            result.append(tens.get(num));
        } else if (num < 100) {
            handleTens(num, result);
        } else if (num < 1000) {
            handleHundreds(num, result);
        } else if (num < 10000) {
            handleThousands(num, result);
        }

        return result.toString();
    }

    private void handleSpecialCase(int num, StringBuilder result) {
        int thousandsPlace = num - (num % 1000);
        int remainder = num % 1000;
        result.append(thousands.get(thousandsPlace));
        if (remainder != 0) {
            result.append(" ");
            if (remainder < 100) {
                handleHundredsAndTens(remainder, result);
            } else {
                handleThousandsHundredsAndTens(remainder, result);
            }
        }
    }

    private void handleHundredsAndTens(int remainder, StringBuilder result) {
        int hundredsPlace = remainder - (remainder % 100);
        int tensPlace = remainder % 100;
        result.append(hundreds.get(hundredsPlace));
        if (tensPlace != 0) {
            result.append(" ");
            if (tensPlace < 10) {
                result.append(units.get(tensPlace));
            } else if (tensPlace < 17 || (tensPlace < 70 && tensPlace % 10 == 0)) {
                result.append(tens.get(tensPlace));
            } else {
                int remainderTens = tensPlace % 10;
                int remainderTensPlace = tensPlace - remainderTens;
                result.append(tens.get(remainderTensPlace));
                if (remainderTens != 0) {
                    result.append(" et ").append(units.get(remainderTens));
                }
            }
        }
    }

    private void handleThousandsHundredsAndTens(int remainder, StringBuilder result) {
        int thousandsHundredsPlace = remainder - (remainder % 100);
        int thousandsTensPlace = remainder % 100;
        result.append(hundreds.get(thousandsHundredsPlace));
        if (thousandsTensPlace != 0) {
            result.append(" ");
            if (thousandsTensPlace < 10) {
                result.append(units.get(thousandsTensPlace));
            } else if (thousandsTensPlace < 17 || (thousandsTensPlace < 70 && thousandsTensPlace % 10 == 0)) {
                result.append(tens.get(thousandsTensPlace));
            } else {
                int remainderTens = thousandsTensPlace % 10;
                int remainderTensPlace = thousandsTensPlace - remainderTens;
                result.append(tens.get(remainderTensPlace));
                if (remainderTens != 0) {
                    result.append(" et ").append(units.get(remainderTens));
                }
            }
        }
    }

    private void handleTens(int num, StringBuilder result) {
        int remainder = num % 10;
        int tensPlace = num - remainder;
        result.append(tens.get(tensPlace));
        if (remainder != 0) {
            result.append("-").append(units.get(remainder));
        }
    }

    private void handleHundreds(int num, StringBuilder result) {
        int hundredsPlace = num - (num % 100);
        int remainder = num % 100;
        result.append(hundreds.get(hundredsPlace));
        if (remainder != 0) {
            result.append(" ");
            if (remainder < 10) {
                result.append(units.get(remainder));
            } else if (remainder < 17 || (remainder < 70 && remainder % 10 == 0)) {
                result.append(tens.get(remainder));
            } else {
                int remainderTens = remainder % 10;
                int remainderTensPlace = remainder - remainderTens;
                result.append(tens.get(remainderTensPlace));
                if (remainderTens != 0) {
                    result.append("-").append(units.get(remainderTens));
                }
            }
        }
    }

    private void handleThousands(int num, StringBuilder result) {
        int thousandsPlace = num - (num % 1000);
        int remainder = num % 1000;
        result.append(thousands.get(thousandsPlace));
        if (remainder != 0) {
            result.append(" ");
            if (remainder < 100) {
                handleHundredsAndTens(remainder, result);
            } else {
                handleThousandsHundredsAndTens(remainder, result);
            }
        }
    }
}
