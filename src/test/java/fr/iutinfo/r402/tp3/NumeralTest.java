package fr.iutinfo.r402.tp3;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class NumeralTest {

  @Test
  void testSingleDigit() {
    Numeral n = new Numeral("5");
    assertEquals("cinq", n.toLetters());
  }

  @Test
  void testTens() {
    Numeral n = new Numeral("20");
    assertEquals("vingt", n.toLetters());
  }

  @Test
  void testTensWithUnits() {
    Numeral n = new Numeral("23");
    assertEquals("vingt-trois", n.toLetters());
  }

  @Test
  void testHundreds() {
    Numeral n = new Numeral("300");
    assertEquals("trois cents", n.toLetters());
  }

  @Test
  void testHundredsWithUnits() {
    Numeral n = new Numeral("315");
    assertEquals("trois cents quinze", n.toLetters());
  }

  @Test
  void testThousands() {
    Numeral n = new Numeral("4000");
    assertEquals("quatre mille", n.toLetters());
  }

  @Test
  void testThousandsWithUnits() {
    Numeral n = new Numeral("4321");
    assertEquals("quatre mille trois cents vingt et un", n.toLetters());
  }
}